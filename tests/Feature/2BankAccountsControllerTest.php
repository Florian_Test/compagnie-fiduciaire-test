<?php

namespace Tests\Feature;

use App\BankAccounts;
use App\Http\Controllers\BankAccountsController;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;


class testBankAccountsController extends BankAccountsController {
    public function method_to_test($method, $param1 = null, $param2 = null, $param3 = null){
        return $this->$method($param1, $param2, $param3);
    }
}
class BankAccountsControllerTest extends TestCase
{

    protected $session_datas;
    protected $api_url;
    protected $class;

    public function setUp()
    {
        parent::setUp();
        $this->session_datas = serialize(json_decode('{
            "statut":"OK",
            "operations":[
                {"RIB":"1","Date":"2017-03-28","Libelle":"Achat","Montant":"-140,83","Devise":"Euro"},
                {"RIB":"2","Date":"2017-03-30","Libelle":"Virement","Montant":"400,00","Devise":"Euro"},
                {"RIB":"2","Date":"2017-04-14","Libelle":"Achat 2","Montant":"-50,00","Devise":"Euro"},
                {"RIB":"2","Date":"2017-04-13","Libelle":"Achat 3","Montant":"-50,00","Devise":"Euro"},
                {"RIB":"2","Date":"2017-03-29","Libelle":"Virement","Montant":"100,00","Devise":"Euro"},
                {"RIB":"3","Date":"2017-04-15","Libelle":"Virement","Montant":"1000,00","Devise":"Euro"}
            ]            
        }'));
        $this->api_url = 'https://agrcf.lib.id/exercice@dev/';
        $this->class = new testBankAccountsController();

    }

    /**
     * Test de la fonction returnErrorServeur()
     */
    public function testReturnErrorServeur(){
        // Appel de la methode à tester
        $response = $this->class->method_to_test('returnErrorServeur');
        // Doit renvoyer un code de redirection (permanente ou non permanente ?)
        $this->assertEquals(302, $response->getStatusCode());
        // Doit renvoyer la vue welcome
        $this->assertEquals(route('welcome'), $response->getTargetUrl());
    }

    public function testIndexWithoutSession(){

        $response = $this->call('GET', route('bank-accounts'));
        // Avoir un statut 200
        $response->assertStatus(200);
        // Avoir les variables $listRIB et $listDates
        $response->assertViewHas('listRIB');
        $response->assertViewHas('listDates');
        // le formulaire doit renvoyer vers la route('bank-accounts-detail')
        $response->assertSee(route('bank-accounts-detail'));
        // session('api_response') doit exister et doit être une string
        $response->assertSessionHas('api_response');
        $this->assertTrue(is_string(session('api_response')));
    }

    /**
     * Test fonction index avec session('api_response') déjà stockée
     */
    public function testIndexWithSession(){

        session()->put('api_response', $this->session_datas);
        //dd(session('api_response'));
        $response = $this->call('GET', route('bank-accounts'));
        // Avoir un statut 200
        $response->assertStatus(200);
        // Renvoyer la vue bank-accounts.index
        $response->assertViewIs('bank-accounts.index');
        // Avoir les variables $listRIB et $listDates
        $response->assertViewHas('listRIB');
        $response->assertViewHas('listDates');
        // le formulaire doit renvoyer vers la route('bank-accounts-detail')
        $response->assertSee(route('bank-accounts-detail'));
    }

    /**
     * Test fonction store par envoi requête en post avec de bons paramètres
     */
    public function testStoreWithGoodParameters(){
        $bank_account = '0';
        $date_start = '2017-04-13';
        $date_end = '2017-04-14';

        // Envoi du formulaire
        $response = $this->post(route('bank-accounts-detail'), ['bank_account' => $bank_account, 'date_start' => $date_start, 'date_end' => $date_end]);
        $response->assertStatus(200);
        // Avoir les variables $bankOperations et $bankSolde
        $response->assertViewHas(['bankOperations']);
        $response->assertViewHas(['bankSolde']);
        $response->assertViewIs('bank-accounts.detail');
        // session('api_response') doit être unseté
        $response->assertSessionMissing('api_response');
    }

    /**
     * Test fonction store par envoi requête en post avec de mauvaises dates
     */
    public function testStoreWithWrongParameters(){
        // Simulation des paramètres passés par formulaires date_end < date_start
        $bank_account = '0';
        $date_start = '2017-04-14';
        $date_end = '2017-04-13';

        // Envoi du formulaire
        $response = $this->post(route('bank-accounts-detail'), ['bank_account' => $bank_account, 'date_start' => $date_start, 'date_end' => $date_end]);
        // La réponse doit être une redirection
        $response->assertStatus(302);
        // Il doit y avoir un message d'erreur pour date_end
        $response->assertSessionHasErrors(['date_end']);

    }

}

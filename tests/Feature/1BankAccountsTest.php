<?php

namespace Tests\Feature;

use App\BankAccounts;
use Illuminate\Http\RedirectResponse;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;


 /*
 * Class testBankAccounts
 *
 * Permet de tester les methodes protected ou private pour avoir un coverage complet
 */
class testBankAccounts extends BankAccounts {
    public function method_to_test($method, $param1 = null, $param2 = null, $param3 = null){
        return $this->$method($param1, $param2, $param3);
    }
}


class BankAccountsTest extends TestCase
{
    private $class;
    public $api_url;
    public $api_response;
    public $bank_operations;
    public $api_datas;

    public function setUp()
    {
        parent::setUp();
        $this->class = new testBankAccounts();
        $this->api_url = 'https://agrcf.lib.id/exercice@dev/';
        $this->api_response = '{
            "statut":"OK",
            "operations":[
                {"RIB":"1","Date":"28/03/2017","Libelle":"Achat","Montant":"-140,83","Devise":"Euro"},
                {"RIB":"2","Date":"30/03/2017","Libelle":"Virement","Montant":"400,00","Devise":"Euro"},
                {"RIB":"2","Date":"14/04/2017","Libelle":"Achat 2","Montant":"-50,00","Devise":"Euro"},
                {"RIB":"2","Date":"13/04/2017","Libelle":"Achat 3","Montant":"-50,00","Devise":"Euro"},
                {"RIB":"2","Date":"29/03/2017","Libelle":"Virement","Montant":"100,00","Devise":"Euro"},
                {"RIB":"3","Date":"15/04/2017","Libelle":"Virement","Montant":"1000,00","Devise":"Euro"}
            ]
        }';
        $this->bank_operations = collect(json_decode($this->api_response)->operations);

    }


    /**
     * Test method checkApiResponse() avec une api_response null
     */
    public function testcheckApiResponseNull(){
        // Mauvais retour
        $api_response = null;
        $api_response_bool = $this->class->method_to_test('checkApiResponse', $api_response);
        // La method doit retourner un boolean
        $this->assertTrue(is_bool($api_response_bool));
        // la methode doit retourner false
        $this->assertFalse($api_response_bool);

    }

    /**
     * Test method checkApiResponse() avec api_datas ne contenant pas la clé statut
     */
    public function testcheckApiResponseWithoutStatutKey(){
        // Mauvais retour
        $api_response = '{"wrong-statut":"OK","operations":[{"RIB":"1","Date":"28/03/2017","Libelle":"Achat","Montant":"-140,83","Devise":"Euro"}]}';
        $api_datas = json_decode($api_response);
        $api_response_bool = $this->class->method_to_test('checkApiResponse', $api_response, $api_datas);
        // La method doit retourner un boolean
        $this->assertTrue(is_bool($api_response_bool));
        // La method doit retourner false
        $this->assertFalse($api_response_bool);

    }

    /**
     * Test method checkApiResponse() avec api_datas->operations ne contenant pas la clé RIB
     */
    public function testcheckApiResponseWithoutWrongOperationsKey(){
        // Mauvais retour
        $api_response = '{"statut":"OK","operations":[{"RIZZZZ":"1","Date":"28/03/2017","Libelle":"Achat","Montant":"-140,83","Devise":"Euro"}]}';
        $api_datas = json_decode($api_response);
        $api_response_bool = $this->class->method_to_test('checkApiResponse', $api_response, $api_datas);
        // La method doit retourner un boolean
        $this->assertTrue(is_bool($api_response_bool));
        // La method doit retourner false
        $this->assertFalse($api_response_bool);

    }

    /**
     * Test method checkApiResponse() avec api_datas ne contenant pas la valeur 'OK' dans l'attribut "statut"
     */
    public function testcheckApiResponseWithWrongStatutKeyResponse(){
        // Mauvais retour
        $api_response = '{"statut":"PAS","operations":[{"RIB":"1","Date":"28/03/2017","Libelle":"Achat","Montant":"-140,83","Devise":"Euro"}]}';
        $api_datas = json_decode($api_response);
        $api_response_bool = $this->class->method_to_test('checkApiResponse', $api_response, $api_datas);
        // La method doit retourner un boolean
        $this->assertTrue(is_bool($api_response_bool));
        // La method doit retourner false
        $this->assertFalse($api_response_bool);
    }

    /**
     * Test method getApiResponse() avec la bonne url
     */
    public function testGetApiResponseWithGoodUrl(){

        $this->class->api_url = $this->api_url;
        $api_response = $this->class->method_to_test('getApiResponse');
        // la réponse de l'api doit être stockée en session en string
        $checkSession = session('api_response');
        $this->assertTrue(is_string($checkSession));
        // La methode doit être un boolean
        $this->assertTrue(is_bool($api_response));
        // la methode doit renvoyer true
        $this->assertTrue($api_response);
        // la réponse de l'api unserialisée doit être un objet
        $api_datas = unserialize($checkSession);
        $this->assertTrue(is_object($api_datas));
        // la réponse doit contenir un statut et 'operation
        $this->assertObjectHasAttribute('statut', $api_datas);
        $this->assertObjectHasAttribute('operations', $api_datas);
        // le statut doit être 'OK'
        $this->assertEquals('OK', $api_datas->statut);
    }

    /**
     * Test method getApiResponse() avec une mauvaise bonne url
     */
    public function testGetApiResponseWithWrongApiUrl(){
        $this->class->api_url = 'https://wrong-url';
        $this->class->api_response = '';
        $api_response = $this->class->method_to_test('getApiResponse');
        // La method doit retourner un boolean
        $this->assertTrue(is_bool($api_response));
        // la methode doit retourner false
        $this->assertFalse($api_response);
    }

    /**
     * Test fonction getBankAccountsList() avec 1 opération
     */
    public function testGetBankAccountsList(){
        $this->class->bank_operations = $this->bank_operations;
        $bankAccountsList = $this->class->method_to_test('getBankAccountsList');
        // la method doir retourner un objet
        $this->assertTrue(is_object($bankAccountsList));
        // la method doit retourner 1 seul RIB
        $this->assertEquals(3, count($bankAccountsList));
    }

    /**
     * Test fonction getBankAccountsList() avec 2 opérations sur 2 Ribs
     */
    public function testGetBankAccountsListWithTwoRibs(){
        $api_response = '{
            "statut":"OK",
            "operations":[
                {"RIB":"1","Date":"28/03/2017","Libelle":"Achat","Montant":"-140,83","Devise":"Euro"},
                {"RIB":"2","Date":"29/03/2017","Libelle":"Virement","Montant":"100,00","Devise":"Euro"}
            ]
        }';
        // Initialisation de l'attribut bank_operations
        $this->class->bank_operations = collect(json_decode($api_response)->operations);
        // Appel de la methode à tester
        $bankAccountsList = $this->class->method_to_test('getBankAccountsList');
        // La method doit retourner un objet
        $this->assertTrue(is_object($bankAccountsList));
        // la method doit retourner 1 seul RIB
        $this->assertEquals(2, count($bankAccountsList));
    }

    /**
     * Test fonction getBankAccountsList() avec 3 opérations sur 2 Ribs
     */
    public function testGetBankAccountsListWithTwoRibsButThreeOperations(){
        $api_response = '{
            "statut":"OK",
            "operations":[
                {"RIB":"1","Date":"28/03/2017","Libelle":"Achat","Montant":"-140,83","Devise":"Euro"},
                {"RIB":"2","Date":"29/03/2017","Libelle":"Virement","Montant":"100,00","Devise":"Euro"},
                {"RIB":"2","Date":"30/03/2017","Libelle":"Virement","Montant":"400,00","Devise":"Euro"}
            ]
        }';
        // Initialisation de l'attribut bank_operations
        $this->class->bank_operations = collect(json_decode($api_response)->operations);
        // Appel de la methode à tester
        $bankAccountsList = $this->class->method_to_test('getBankAccountsList');
        // la method doit retourner un objet
        $this->assertTrue(is_object($bankAccountsList));
        // la method doit retourner 2 RIBs
        $this->assertEquals(2, count($bankAccountsList));

    }

    /**
     * Test fonction getOperationsDates() avec 3 opérations
     */
    public function testGetOperationsDates(){
        $api_response = '{
            "statut":"OK",
            "operations":[
                {"RIB":"1","Date":"28/03/2017","Libelle":"Achat","Montant":"-140,83","Devise":"Euro"},
                {"RIB":"2","Date":"29/03/2017","Libelle":"Virement","Montant":"100,00","Devise":"Euro"},
                {"RIB":"2","Date":"30/03/2017","Libelle":"Virement","Montant":"400,00","Devise":"Euro"}
            ]
        }';
        $this->class->bank_operations = collect(json_decode($api_response)->operations);
        $listDates = $this->class->method_to_test('getOperationsDates');
        // la method doit retourner un tableau
        $this->assertTrue(is_array($listDates));
        // Le tableau retourné doit contenir les clés date_start et date_end
        $this->assertArrayHasKey('date_start', $listDates);
        $this->assertArrayHasKey('date_end', $listDates);
        // la methode doit retourner pour date_start '28/03/2017'
        $this->assertEquals('28/03/2017', $listDates['date_start']);
        // La methode doit retourner pour date_end '30/03/2017'
        $this->assertEquals('30/03/2017', $listDates['date_end']);
    }

    public function testGetRibNumber(){
        $this->class->bank_operations = $this->bank_operations;
        $account = 0;
        $rib_number = $this->class->method_to_test('getRibNumber', $account);
        // La method doit retourner une string
        $this->assertTrue(is_string($rib_number));
        // La method doit retourner "1" le numéro de compte de la seule ecriture $this->bank_operations
        $this->assertEquals("1", $rib_number);
    }

    public function testGetOperationsDatas(){
        // Reconstitution de api_datas avec format date Y-m-d TODO: besoin de refactoring de la fonction getOperationDatas();
        $this->api_datas = json_decode($this->api_response);
        array_map(function($item){
            $item->Date = str_replace('/', '-', $item->Date);
            $item->Date = date('Y-m-d', strtotime($item->Date));
            return $item;
        }, $this->api_datas->operations);

        $this->bank_operations = collect($this->api_datas->operations);
        $this->class->bank_operations = $this->bank_operations;

        $bankAccountsList = $this->class->getBankAccountsList();
        // Récupération de l'id du compte pour le RIB 2 doit être égale à 1
        $account = (string) array_flip($bankAccountsList->toArray())['2'];
        $this->assertEquals(1, $account);
        $date_start = '2017-03-29';
        $date_end = '2017-04-13';
        $operations = $this->class->method_to_test('getOperationsDatas', $account, $date_start, $date_end);
        // La methode doit retourner un objet
        $this->assertTrue(is_object($operations));
        // La methode doit retourner 3 opérations
        $this->assertEquals(3, $operations->count());
        // L'opération la plus ancienne doit dater du 29/03/2017
        $this->assertEquals($operations->first()->Date, '29/03/2017');
        // L'opération la plus recente doit dater du 13/04/2017
        $this->assertEquals($operations->last()->Date, '13/04/2017');
        return $operations;
    }

    public function testGetSoldeBankAccount(){
        $operations = $this->testGetOperationsDatas();
        // La methode doit retourner 450 € (100 + 400 -50)
        $solde = $this->class->method_to_test('getSoldeBankAccount', $operations);
        $this->assertEquals(450, $solde);
    }

    public function test__ConstructWithSession()
    {
        // Création de la propriété api_response dans la session
        session()->put('api_response', serialize(json_decode($this->api_response)));
        // Initialisation de la class
        $class = new testBankAccounts();
        // la method doit retourner une classe avec les attributs api_response et bank_operations
        $this->assertObjectHasAttribute('api_response', $class);
        $this->assertObjectHasAttribute('bank_operations', $class);
        // L'attribut api_response doit être un objet et avoir les clés statut et operations
        $this->assertTrue(is_object($class->api_response));
        $this->assertObjectHasAttribute('statut', $class->api_response);
        $this->assertObjectHasAttribute('operations', $class->api_response);
        // L'attribut bank_operation doit être un objet
        $this->assertTrue(is_object($class->bank_operations));

    }
}

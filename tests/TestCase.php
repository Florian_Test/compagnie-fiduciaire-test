<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    // Fix le chargement en double des constantes dans l'environnemnet de test
    protected $preserveGlobalState = FALSE;
    protected $runTestInSeparateProcess = TRUE;
}

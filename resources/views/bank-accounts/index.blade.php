@extends('layouts.app')

@section('content')

    {{Form::open(['url' => route('bank-accounts-detail')])}}
        @csrf
        <div style="display:flex;">
            <div>
                {{Form::select('bank_account', $listRIB, null, ['placeholder' => 'Selectionner un RIB', 'required', 'class' => 'form-control form-control-sm'])}}
            </div>
            <div>
                {{Form::date('date_start', $listDates['date_start'], ['min' => $listDates['date_start'], 'max' => $listDates['date_end'], 'class' => 'form-control form-control-sm'])}}
            </div>
            <div>
                <div>
                    {{Form::date('date_end', $listDates['date_end'], ['min' => $listDates['date_start'], 'max' => $listDates['date_end'], 'class' => 'form-control form-control-sm'])}}
                </div>
                <div>
                    @if($errors->has('date_end'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{!! $errors->first('date_end') !!}</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <br>
        {{Form::submit('Afficher détail opérations', ['class' => 'btn btn-primary'])}}

    {{Form::close()}}

@endsection

@extends('layouts.app')

@push('styles')
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div>
        <div>Details du compte n° {{$bankNumber}}</div>
        <div>Période du {{$date_start}} au {{$date_end}}</div>
        <div>Solde sur la période : <strong>{{number_format($bankSolde, '2', ',', ' ')}} €</strong></div>
    </div>
    <div style="display: inline-block;">
        <a class="btn btn-info btn-sm" href="{{route('bank-accounts')}}">Retour</a>
        <a class="btn btn-info btn-sm" href="{{route('welcome')}}">Acceuil</a>
    </div>
    <br>
    <table class="table table-striped table-sm table-bordered table-dark">
        <thead>
        <tr>
            <th>Date</th>
            <th>Libelle</th>
            <th>Depense</th>
            <th>Recette</th>
            <th>Montant</th>
        </tr>
        </thead>
        <tbody>
            @foreach($bankOperations as $operation)
                <tr>
                    <th>{{$operation->Date}}</th>
                    <th>{{$operation->Libelle}}</th>
                    <th>{{number_format($operation->Depense, '2', ',', ' ')}}</th>
                    <th>{{number_format($operation->Recette, '2', ',', ' ')}}</th>
                    <th>{{number_format($operation->Montant, '2', ',', ' ')}}</th>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="2">Solde : <strong>{{number_format(($bankOperations->sum('Recette') - $bankOperations->sum('Depense')), '2', ',' , ' ')}} €</strong></th>
                <th>{{number_format($bankOperations->sum('Depense'), '2', ',', ' ')}}</th>
                <th>{{number_format($bankOperations->sum('Recette'), '2', ',', ' ')}}</th>
                <th>{{number_format($bankOperations->sum('Montant'), '2', ',', ' ')}}</th>
            </tr>
        </tfoot>
    </table>
@endsection

@push('script')
    <script src="{{ asset('js/datatables.min.js') }}" ></script>

    {{-- Initialisation du datatable --}}
    <script>
        $(document).ready( function () {
            $('.table').dataTable({
                autoWidth: true,
                info: false,
                paging: false,
                lengthMenu : false,
                language: {
                    "infoEmpty": "Aucune donnée correspondante à la recherche",
                    "emptyTable": "Aucune donnée correspondante à la recherche",
                    "search": 'Recherche : ',
                },
            });
        })
    </script>
@endpush


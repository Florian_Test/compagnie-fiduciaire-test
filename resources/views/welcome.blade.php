@extends('layouts.app')

@section('content')
    <div class="row">
        <a href="{{route('bank-accounts')}}" class="btn btn-dark">Détail Comptes bancaires</a>
    </div>
    <br>
    <div class="row">
        @if ($errors->has('error-json'))
            <div class="alert alert-danger" role="alert">
                <strong>{!! $errors->first('error-json') !!}</strong>
            </div>
        @endif
    </div>
@endsection

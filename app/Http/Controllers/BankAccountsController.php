<?php

namespace App\Http\Controllers;

use App\BankAccounts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BankAccountsController extends Controller
{
    public $api_response;
    public $api_url = 'https://agrcf.lib.id/exercice@dev/';

    public function __construct(){
        // Utilisation d'un middelware pour accèder à la session depuis le controller
        $this->middleware(function ($request, $next) {
            // Vérifie si la reponse de l'api est déjà chargée en session
            if( session('api_response') === null){
                // Mise en session des données de l'api
                $bankAccounts = new BankAccounts();
                //$this->api_url = $bankAccounts->api_url;
                $this->api_response = $bankAccounts->getApiResponse();
                // Retourn message d'erreur si la réponse de l'api est false
                if($this->api_response === false){
                    $this->returnErrorServeur();
                }
            }
            return $next($request);
        }); // end middleware
    }
    protected function returnErrorServeur(){
        return redirect()->back()->withErrors(['error-json' => 'Le serveur a renvoyé une erreur, veuillez contacter votre administateur.']);
    }
    /**
     * Retourne la vue avec formulaire de selection du RIB et des dates de la période à afficher
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $bankAccounts = new BankAccounts();

        // Initialisation des variables à passer à la vue
        $listRIB = $bankAccounts->getBankAccountsList();
        $listDates = $bankAccounts->getOperationsDates();

        return view('bank-accounts.index', compact('listRIB', 'listDates'));

    }

    /**
     * Retourne la vue avec le détails des écritures bancaires
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        // Validation du formulaire (champ date end > date debut).
        $validator = Validator::make($request->all(), [
            'date_end' => 'after_or_equal:date_start'
        ], ['date_end.after_or_equal' => 'La date de fin doit être supérieure ou égale à la date de début.']);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }
        // Initialisation des variables à passer à la vue bank-accounts.detail
        $bankAccounts = new BankAccounts();
        $bankOperations = $bankAccounts->getOperationsDatas($request->bank_account, $request->date_start, $request->date_end);
        $bankNumber = $bankOperations->first()->RIB;
        $bankSolde = $bankAccounts->getSoldeBankAccount($bankOperations);
        $date_start = date('d/m/Y' , strtotime($request->date_start));
        $date_end = date('d/m/Y' , strtotime($request->date_end));

        // Suppression des données mises en session
        session()->forget('api_response');

        return view('bank-accounts.detail', compact('bankOperations', 'bankNumber', 'bankSolde', 'date_start', 'date_end'));
    }
}

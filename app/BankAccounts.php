<?php

namespace App;

use Illuminate\Support\Collection;

class BankAccounts
{
    public $api_response;
    public $bank_operations;
    public $api_url = 'https://agrcf.lib.id/exercice@dev/';
    public $api_datas;

    public function __construct(){
        // Si la reponse de l'api est chargée en session, initialisation Collection avec les opérations bancaires
        if(session('api_response')){
            $this->api_response = unserialize(session('api_response'));
            $this->bank_operations = collect($this->api_response->operations);
        }
    }

    /**
     * Recupère le json del'api
     * @return bool
     */
    protected function callApi():bool{
        try{
            $this->api_response = file_get_contents($this->api_url);
        }catch(\Exception $e){
            return false;
        }
        return true;
    }

    /**
     * Récupére les données de l'api et mise en session si aucune erreur détéctée
     * @return bool
     */
    public function getApiResponse():bool
    {
        // Récupération reponse api
        if($this->callApi() === false){
            return false;
        };
        $this->api_datas = json_decode($this->api_response);
        // Vérification de la réponse
        $this->checkApiResponse($this->api_response, $this->api_datas);

        // Formatage du format des dates 'Y-m-d' des operations bancaires
        array_map(function($item){
            $item->Date = str_replace('/', '-', $item->Date);
            $item->Date = date('Y-m-d', strtotime($item->Date));
        }, $this->api_datas->operations);

        // Mise en session des données pour éviter de charger l'api à plusieurs reprises
        session()->put('api_response', serialize($this->api_datas));

        return true;
    }


    /**
     * Vérifie si la réponse de l'api est null ou ne contient pas les clés "statut" et "operations"
     * @param String|null $api_response
     * @param \stdClass |null $api_datas
     * @return bool
     */
    protected function checkApiResponse(String $api_response = null, ?\stdClass $api_datas):bool
    {
        // Si le json renvois null
        if($api_response === null){
            return false;
        }
        // Si le json ne contient pas les clés "statut" et "operations"
        if(!isset($api_datas->statut) || !isset($api_datas->operations)){
            return false;
        }
        // Si la clé statut du json n'est pas 'OK'
        if($api_datas->statut !== "OK"){
            return false;
        }
        //  Si les clés "RIB", "Date", "Montant" et "Libelle" ne sont pas definies dans le json
        $operation = $api_datas->operations[0];
        if(isset($operation->RIB, $operation->Date, $operation->Montant, $operation->Libelle) === false){
            return false;
        }
        return true;
    }

    /**
     * Retourne la liste des RIBs triés par ordre croissant
     * @return Collection
     */
    public function getBankAccountsList():collection{
        return $this->bank_operations->sortBy('RIB')->groupBy('RIB')->keys();
    }

    /**
     * Retourne les dates min et max des opérations bancaires
     * @return array
     */
    public function getOperationsDates():array{
        $listDates = [];
        $listDates['date_start'] = $this->bank_operations->min('Date');
        $listDates['date_end'] = $this->bank_operations->max('Date');
        return $listDates;

    }

    /**
     * Retourne une collection avec les opérations bancaires concernant le RIB et la période sélectionnés
     *
     * @param String $account
     * @param String $date_start
     * @param String $date_end
     * @return Collection
     */
    public function getOperationsDatas(String $account, String $date_start, String $date_end):collection
    {
        $rib_number = $this->getRibNumber($account);
        // Séléction des opérations concernées et formatage données pour retour vue
        $operations = $this->bank_operations
            ->where('RIB', '=', $rib_number)
            ->where('Date', '>=', $date_start)
            ->where('Date', '<=', $date_end)
            ->sortBy('Date')
            ->map(function($item){
                // Formatage date 'd/m/Y'
                $item->Date = date('d/m/Y', strtotime($item->Date));
                $item->Montant = number_format((float) $item->Montant,2);
                // Ajout propriété Depense et Recette à la collection
                if($item->Montant > 0.00){
                    // Le montant est une recette
                    $item->Recette = (float) number_format((float) $item->Montant,2);
                    $item->Depense = 0.00;
                }else{
                    // Le montant est une dépense ou égal à 0
                    $item->Recette = (float) 0.00;
                    $item->Depense = number_format((float) $item->Montant,2) * -1;
                }
                return $item;
            });

        return $operations;
    }

    /**
     * Retourn le numéro du RIB
     * @param String $account
     * @return string
     */
    public function getRibNumber(String $account):string{
        return $this->getBankAccountsList()[$account];
    }


    /**
     * Retourne le solde des opérations bancaires passées en paramètre
     * @param Collection $bankOperations
     * @return string
     */
    public function getSoldeBankAccount(Collection $bankOperations):string
    {
        $total_recettes = (float) $bankOperations->sum('Recette');
        $total_depenses = (float) $bankOperations->sum('Depense');
        return $total_recettes - $total_depenses;
    }
}

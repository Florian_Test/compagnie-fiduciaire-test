<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
})->name('welcome');

//Auth::routes();

Route::get('/bank-accounts', 'BankAccountsController@index')->name('bank-accounts');
Route::post('/bank-accounts', 'BankAccountsController@store')->name('bank-accounts-detail');

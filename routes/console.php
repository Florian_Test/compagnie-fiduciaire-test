<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/


// PHP Unit Coverage (audit avec rendu visuel en HTML)
Artisan::command('phpunit:coverage', function () {
    $this->info('Démarrage de PHP Unit Avec Coverage');
    $cmd = 'env ENV=dev .' . DS . 'vendor' .DS . 'bin' . DS . 'phpunit --coverage-html .'.DS . 'tests' . DS . 'coverage';
    $process = new \Symfony\Component\Process\Process($cmd);
    $process->run();
    $this->info('Génération reporting audit du code effectué. Voir /tests/coverage/index.html');
})->describe('Execute PHP Unit avec Coverage HTML');
